===== STAFF =====

1. CREATE USER bli_staff_admin IDENTIFIED BY password123;
2. GRANT CONNECT, RESOURCE, DBA TO bli_staff_admin;
3. Jalanin schema bli_staff.sql

===== STALL ====

1. CREATE USER bli_stall_admin IDENTIFIED BY password123;
2. GRANT CONNECT, RESOURCE, DBA TO bli_stall_admin;
3. Jalanin schema bli_stall.sql